﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Escola.Models
{
    public class Aluno
    {
        public int ID { get; set; }
        [Required, Display(Name="Nome próprio"), StringLength(50, MinimumLength=2, ErrorMessage="Entre 2-50 caracteres"), RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$")]
        public string Nome { get; set; }
        [Required, Display(Name = "Nome de família"), StringLength(50, MinimumLength = 2, ErrorMessage = "Entre 2-50 caracteres"), RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$")]
        public string Pronome { get; set; }
        public string NomeCompleto
        {
            get
            {
                return Nome + " " + Pronome;
            }
        }
        virtual public IList<Inscrição> Inscrições { get; set; }
    }
}