﻿using Escola.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Escola.DAL
{
    public class EscolaContext : DbContext
    {
        public EscolaContext() : base("EscolaContext")
        {
        }
        public DbSet<UC> UCs { get; set; }
        public DbSet<Departamento> Departamentos { get; set; }
        public DbSet<Inscrição> Inscrições { get; set; }
        public DbSet<Aluno> Alunos { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}