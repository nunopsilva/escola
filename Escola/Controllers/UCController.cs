﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Escola.Models;
using Escola.DAL;

namespace Escola.Controllers
{
    public class UCController : Controller
    {
        private EscolaContext db = new EscolaContext();

        // GET: /UC/
        public ActionResult Index()
        {
            var UCs = db.UCs.Include(c => c.Departamento);
            return View(UCs.ToList());
        }

        private void PopulateDepartamentosDropDownList(object selectedDepartamento = null)
        {
            var departamentosQuery = from d in db.Departamentos
                                     orderby d.Sigla
                                     select d;

            ViewBag.DepartamentoID = new SelectList(departamentosQuery, "ID", "Sigla", selectedDepartamento);
        }

        // GET: /UC/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UC uc = db.UCs.Find(id);
            if (uc == null)
            {
                return HttpNotFound();
            }
            return View(uc);
        }

        // GET: /UC/Create
        public ActionResult Create()
        {
            PopulateDepartamentosDropDownList();
            return View();
        }

        // POST: /UC/Create
		// To protect from over posting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		// 
		// Example: public ActionResult Update([Bind(Include="ExampleProperty1,ExampleProperty2")] Model model)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Sigla,Créditos,DepartamentoID")]UC uc)
        {
            if (ModelState.IsValid)
            {
                db.UCs.Add(uc);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            PopulateDepartamentosDropDownList(uc.DepartamentoID);
            return View(uc);
        }

        // GET: /UC/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UC uc = db.UCs.Find(id);
            if (uc == null)
            {
                return HttpNotFound();
            }

            PopulateDepartamentosDropDownList(uc.DepartamentoID);
            return View(uc);
        }

        // POST: /UC/Edit/5
		// To protect from over posting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		// 
		// Example: public ActionResult Update([Bind(Include="ExampleProperty1,ExampleProperty2")] Model model)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID, Sigla,Créditos,DepartamentoID")]UC uc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uc).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            PopulateDepartamentosDropDownList(uc.DepartamentoID);
            return View(uc);
        }

        // GET: /UC/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UC uc = db.UCs.Find(id);
            if (uc == null)
            {
                return HttpNotFound();
            }
            return View(uc);
        }

        // POST: /UC/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UC uc = db.UCs.Find(id);
            db.UCs.Remove(uc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
